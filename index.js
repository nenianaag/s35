const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config()

const app = express()
const port = 3001

// MongoDB connection

// console.log(process.env.MONGODB_PASSWORD)

mongoose.connect(`mongodb+srv://ninznaag:${process.env.MONGODB_PASSWORD}@cluster0.niv71rw.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection

db.on('error', () => console.error("Connection error."))
db.on('open', () => console.log("Connected to MongoDB!"))

// MongoDB connection END

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Schemas
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
})
// MongoDB Schemas END

// 1. Create a User Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String	
})

// MongoDB Model
const Task = mongoose.model('Task', taskSchema)

// 2. Create a User Model
const User = mongoose.model('User', userSchema)

// MongoDB Model END

// Routes
app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}, (error, result) => {
		if(result !== null && result.name == request.body.name){
			return response.send('Duplicate task found!')
		}

		let newTask = new Task({
			name: request.body.name
		})

		newTask.save((error, savedTask) => {
			if(error){
				return console.error(error)
			}
			else {
				return response.status(200).send('New task created!')
			}
		})
	})
})

app.get('/tasks', (request, response) => {
	Task.find({}, (error, result) => {
		if(error){
			return console.log(error)
		}

		return response.status(200).json({
			data: result
		})
	})
})

// 3. Create POST route that will access the /signup route that will create a user.
app.post('/signup', (request, response) => {
	
	return User.find({username: request.body.username}, (error, user) => {

		console.log(user)
		if (user.length == 0){
			let newUser = new User({
				username: request.body.username,
				password: request.body.password
			})

			newUser.save((error, savedUser) => {
				if(error){
					return console.error(error)
				}
				else {
					return response.status(200).send('New user registered!')
				}
			})
		} else {
			return response.status(200).send('Duplicate user found!')	
		}	
	})	
})

// Routes END

app.listen(port, () => console.log(`Server running at localhost:${port}`))
	